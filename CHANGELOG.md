# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.1] - 2017-12-11

### Changed

- Hide CHANGELOG from pypi and move to markdown again

## [1.1.0] - 2017-12-11

### Changed


- Use `repool-forked` instead of `repool`

## [1.0.4] - 2017-11-28

### Fixed

- CHANGELOG.rst syntax to render on pypi

## [1.0.3] 2017-11-28

### Fixed

- CHANGELOG.rst include in package

## [1.0.2]- 2017-11-28

### Fixed

- Bumpversion configuration

## [1.0.1] - 2017-11-28

### Changed

- Changelog format to RestructuredText

## [1.0.0] - 2017-11-28

### Added

- Moved from core repository to separate plugin
- Pylint support
- Mypy support
- PyCodeStyle support
- Makefile with check command

[1.1.1]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.1.0..v1.1.1
[1.1.0]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.0.4..v1.1.0
[1.0.4]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.0.3..v1.0.4
[1.0.3]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.0.2..v1.0.3
[1.0.2]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.0.1..v1.0.2
[1.0.1]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/compare/v1.0.0..v1.0.1
[1.0.0]: https://gitlab.com/AnjiProject/errbot-rethinkdb-storage/commits/v1.0.0