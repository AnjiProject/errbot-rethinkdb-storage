check:
	-pylint errbot_rethinkdb_storage
	-mypy errbot_rethinkdb_storage
	-pycodestyle errbot_rethinkdb_storage
	-restructuredtext-lint CHANGELOG.rst README.rst AUTHORS.rst
release:
	python setup.py sdist upload