============================
RethinkDB Storage for ErrBot
============================


.. image:: https://img.shields.io/pypi/v/errbot_rethinkdb_storage.svg
        :target: https://pypi.python.org/pypi/errbot_rethinkdb_storage

RethinkDB storage plugin for ErrBot


* Free software: MIT license


Features
--------

* Base RethinkDB storage plugin for ErrBot

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

